I am learning data analysis and love running, so I put the two together. Please fell free to comment and make suggestions.
I used Jupyter Notebook, Python and the my running data on Runkeeper. 
Please note: all variables use the metric system. If you use the imperial system, you can add some lines of code and convert it using PyPI: https://pypi.python.org/pypi/units/ 
Because my data was collected over a couple of years, some variables take that into account. It is still possible to use this notebook if you have started tracking your activities recently, you just have to adjust (change years to months or weeks, whatever your case may be) and remember that it might not be representing the 'groundtruth', because there is not enough data yet. 
Have fun!